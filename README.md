# QUBIK mission documentation #

This repository contains the source code of QUBIK mission documentation.

The rendered documentation can be found [here](https://qubik.libre.space/).

## Styling guide ##

reStructeredText in this repository shall follow this styling guide for source code consistency.
These rules are **not** checked as part of quality gating with a CI job.


### Sections ###

The succession of headings shall follow the [Python Developer's Guide for documentation](https://devguide.python.org/documentation/markup/#sections):

<!-- vale Google.Units = NO -->
  * 1st level, `#` with overline
  * 2nd level, `*` with overline
  * 3rd level, `=`
  * 4th level, `-`
  * 5th level, `^`
  * 6th level, `"`
<!-- vale Google.Units = YES -->

### Lists ###

Numbered lists shall be auto-numbered using the `#.` sign, unless required otherwise.
Unnumbered lists shall only use `*` sign, unless required otherwise.


### Paragraphs ###

All sentences shall end with a punctuation mark followed by a newline character.
Lines shall be wrapped to 79 characters (GNU formatting style).


## Linting ##

To lint the source code of the documentation, run:

```
$ tox run-parallel -e doc8,sphinx-lint,vale,linkcheck
```


## Building ##

To build the documentation, run:

```
$ tox run -e docs
```

To re-build in a re-created environment, run:

```
$ tox run -r -e docs
```


## Maintenance ##

Package dependencies for building the documentation are in:

  * `requirements.txt` - Dependencies to build the docs
  * `requirements-dev.txt` - Packages used for development
  * `constraints.txt` - Package version constraints

To update the dependencies, edit the version ranges in `requirements.txt` and `requirements-dev.txt` files, and then run `contrib/refresh-requirements-docker.sh` to regenerate the `constraints.txt` file.


## License ##

[![license](https://img.shields.io/badge/license-CC%20BY--SA%204.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202020-Libre%20Space%20Foundation-6672D8.svg)](https://libre.space/)
