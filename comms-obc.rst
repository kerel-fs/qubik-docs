COMMS and OBC
##############
* Description
* Repository
* Releases

System Perfomance
*****************
* https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw/-/issues/48

System Assembly
***************
Related issue:

* Before Gluing and coating a :doc:`dry fit <pre-assembly>` must be done

System Testing
**************
Related documentation:

* https://gitlab.com/librespacefoundation/pq9ish/pq9ish-comms-vu-hw/-/wikis/home
