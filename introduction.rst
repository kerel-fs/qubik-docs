############
Introduction
############

QUBIK mission is a twin PocketQube satellite mission, designed for amateur radio service and experiment.
The science mission of QUBIK is to experiment with LEOP and passive RF orbit determination.

This is the main documentation of the project.

.. image:: img/exploded.png
   :width: 400px
   :alt: QUBIK Exploded
   :align: center


It is clear that in order to experiment with solutions addressing the :doc:`objectives`, in-orbit-demonstrations should be in place, combining them with experiments on ground segments. Libre Space Foundation has chosen to self-fund an in-orbit demonstrator of an open source in-house developed pocketqube platform (QUBIK).

QUBIK enables us to lower the cost for a COMMS In-Orbit-Demonstrator and quickly get to orbit (expected Nov 2020) verifying some of our assumptions and test out different solutions to address the goals set. We envision QUBIK as a readily-available, accessible and affordable platform for such small-scale, short-lived, small-payload experimentations. That said, LSF also owns and develops technologies and platforms for larger scale requirements (power, mass, operational) through a cubesat platfrom if needed in the near future.

Engage
======

#. Contact the team and keep in touch with mission operations in our `QUBIK Matrix channel <https://app.element.io/?#/room/#qubik:matrix.org>`_ .
#. Help with the :doc:`reception` of QUBIK signals once they are deployed.
#. Are you a radio amateur? :doc:`command` and control the QUBIK satellites!
#. See all our code and documentation in `our open source repositories <https://gitlab.com/librespacefoundation/qubik>`_.
