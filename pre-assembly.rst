Pre Assembly
############

Dry-Fit
*******
Follow the `assembly
guide <https://qubik.libre.space/projects/qubik-assembly-guide/en/latest/>`__
**without** using glues (epoxy and Blue threadlocker). All the parts are
ready and tested **without** glues and conformal
coating.

Known issues:

* Collision between zip-ties of BMPS and ballast board. The guide of BMPS assembly includes the correct method.

.. figure:: img/zip-ties-collision.JPG
   :alt: zip-ties-collision

   Zip-ties collision

* collision between PQ9ish RF connector and antenna cable with side solar panel. In picture is appeared the spot of the antenna cable (near the number 16). A solution in case where the re-soldering of RF connector isn’t possible (due to conformal or limited time)

.. figure:: img/rf-panel-collision-1.JPG
   :alt: rf-panel-collision-1

   RF connector and side panel collision

.. figure:: img/rf-panel-collision-2.JPG
   :alt: rf-panel-collision-2

   RF connector and side panel collision

* Check the countersink of top frame with the screw M2.5x60 DIN965

.. figure:: img/fixed-countersink.JPG
   :alt: fixed-countersink

   Fixed countersink of top frame

*  Check the soldering pad height in PQ9ish bus to be according to `PQ9 standard <https://dataverse.nl/file.xhtml?fileId=11683&version=1.0>`__.

.. figure:: img/soldering.png
   :alt: soldering

   Proper soldering pad height

This problem might drive to not connect properly the sub systems. Also
pay attention in the maximum cycles of
`connector-SQT-109-03-L-S <https://gr.mouser.com/datasheet/2/527/sqt_th-2854651.pdf>`__
which are 100 in dry fit and final assembly process.

It is important to check every change in structural and perform a dry
fit after it. Also the meaning of dry fit is to check the assembly
before apply glues and conformal coating.
